
var loadState = {
    preload: function() {
        game.load.audio('start','start.wav');
        game.load.audio('gamebgm','gamebgm.wav');
        game.load.image('bg', 'bg.png');
        game.load.image('ranking', 'ranking.png');
        game.load.image('menu', 'menu.png');
        game.load.image('wall1', 'wall12.png');
        game.load.image('wall2', 'wall12.png');
        game.load.image('thorn','thorn2.png');
        game.load.image('plat','plat.png');
        game.load.image('end1','end1.png');
        game.load.image('end2','end2.png');
        game.load.spritesheet('player1', '1232.png', 40, 40);
        game.load.spritesheet('player2', '234.png', 40, 40);
        game.load.image('nails', 'nails.png');
        game.load.spritesheet('conveyorRight', 'rightrole.png', 96, 16);
        game.load.spritesheet('b2', 'b2.png', 200, 60);
        game.load.spritesheet('b1', 'b1.png', 200, 60);
        game.load.spritesheet('b3', 'b3.png', 250, 60);
        game.load.spritesheet('b4', 'b4.png', 200, 60);
        game.load.spritesheet('conveyorLeft', 'leftrole.png', 96, 16);
        game.load.spritesheet('trampoline', 'trampoline.png', 96, 22);
        game.load.spritesheet('fake', 'fake.png', 96, 36);
        //sound
        game.load.audio('conveyor','conveyor.wav');
        game.load.audio('fake','fake.wav');
        game.load.audio('nails','nails.wav');
        game.load.audio('plat','plat.wav');
        game.load.audio('trampoline','trampoline.wav');
        game.load.audio('end','end.wav');
        game.load.audio('sound','sound.wav');
        game.load.audio('sound2','sound2.wav');
        game.load.audio('start','start.wav');
        game.load.audio('player1dead','player1dead.wav');
        game.load.audio('player2dead','player2dead.wav');
 
    },
    create: function() {
        game.state.start('menu');
    },
}
;

var game = new Phaser.Game(600, 500, Phaser.AUTO, 'canvas');
game.global = {
    floor:0
   }
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('main', mainState);
game.state.add('main2', mainState2);
game.state.add('end1', endState);
game.state.add('end2', endState2);
game.state.add('rank', rankState);
game.state.start('load');




