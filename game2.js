
var platforms = [];
var lastTime=0;
var player1life=0;
var text1;
var text2;
var text3;
var speed;
var floorct;
var floortime;
var player2life=0;
var key1;
var key2;
var mainState2 = {

    create: function() {

        //character creat
        game.stage.backgroundColor = 'black';
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;
        game.add.image(0, 0,'bg');
        
        this.cursor = game.input.keyboard.createCursorKeys();

        this.player1 = game.add.sprite(game.width/2, game.height/2, 'player1');
        this.player1.anchor.setTo(0.5, 0.5);
        this.player1.facingLeft = false;
        game.physics.arcade.enable(this.player1);
        this.player1.body.gravity.y = 500;
        this.player2 = game.add.sprite(game.width/2+40, game.height/2, 'player2');
        this.player2.anchor.setTo(0.5, 0.5);
        this.player2.facingLeft = false;
        game.physics.arcade.enable(this.player2);
        this.player2.body.gravity.y = 500;
        this.thorn = game.add.sprite(24,0,'thorn');
        this.thorn.scale.setTo(1.4,1.5);

        this.wall1 = game.add.sprite(0,0,'wall1');
        this.wall2 = game.add.sprite(579,0,'wall2');
        var style = {fill: '#0033cc', fontSize: '30px'}
        var style2 = {fill: '#ff0000', fontSize: '30px'}
        key1=game.input.keyboard.addKey(Phaser.Keyboard.Z);
        key2=game.input.keyboard.addKey(Phaser.Keyboard.X);
        game.input.keyboard.removeKeyCapture(Phaser.Keyboard.Z);
        game.input.keyboard.removeKeyCapture(Phaser.Keyboard.X);
        text1 = game.add.text(30, 20, '', style);
        text2 = game.add.text(500, 20, '', style);
        text3 = game.add.text(30, 50, '', style2);

        //player animation
        this.player1.animations.add('rightwalk', [1,2, 3], 8, true); 
        this.player1.animations.add('leftwalk', [1,2,3], 8, true);
        this.player2.animations.add('rightwalk2', [1,2, 3], 8, true); 
        this.player2.animations.add('leftwalk2', [1,2,3], 8, true);  

        var platform;
        platform = game.add.sprite(game.width/2-50, game.height/2+30, 'plat');
        game.physics.arcade.enable(platform);
        platform.body.immovable = true;
        platforms.push(platform);
        platform.body.checkCollision.down = false;
        platform.body.checkCollision.left = false;
        platform.body.checkCollision.right = false;
        //sound
        this.conveyorSound = game.add.audio('conveyor');
        this.fakeSound = game.add.audio('fake');
        this.nailsSound = game.add.audio('nails');
        this.platSound = game.add.audio('plat');
        this.trampolineSound = game.add.audio('trampoline');
        this.endSound = game.add.audio('end');
        this.player1.touchOn=undefined;
        this.player2.touchOn=undefined;
        this.bgmSound = game.add.audio('gamebgm');
        this.playerSound = game.add.audio('sound');
        this.player2Sound = game.add.audio('sound2');
        this.playerdeadSound = game.add.audio('player1dead');
        this.player2deadSound = game.add.audio('player2dead');
        this.bgmSound.loop=true;
        this.bgmSound.play();
        player1life=10;
        player2life=10;
        floor=0;
        floorct=0;
        speed=2;
        floortime=600;
    },

    update: function() {
        if(player1life==0||!this.player1.inWorld){
            this.playerdeadSound.play();
        }
        if(player2life==0||!this.player2.inWorld){
            this.player2deadSound.play();
        }
        if (player1life==0||player2life==0||!this.player1.inWorld||!this.player2.inWorld) { 
            this.bgmSound.stop();
            this.endSound.play();
            platforms.forEach(function(s) {s.destroy()});
            platforms = [];
            game.state.start('end2');
        }
        this.movePlayer();
        this.movePlayer2();
        this.updatePlatforms();
        this.createPlatforms();
        this.physics.arcade.collide(this.player1, platforms, this.effect, null, this);
        this.physics.arcade.collide(this.player2, platforms, this.effect2, null, this);
        text1.setText('血量:' + player1life);
        text3.setText('血量:' + player2life);
        text2.setText(floor+'層');

        if(floorct==10){
            if(speed<=30)speed+=0.2;
            if(floortime>150)floortime-=20;
            floorct=0;
        }
        if(this.player1.body.y<25){
            this.player1.body.velocity.y=50;
            this.player1.body.y+=10;
            if(player1life>3){
                player1life -= 3;
            }else{
                player1life=0;
            }
            game.camera.shake(0.02, 300);
            game.camera.flash(0xff0000, 100);
            this.playerSound.play();
            this.nailsSound.play();

        }
        if(this.player2.body.y<25){
            this.player2.body.velocity.y=50;
            this.player2.body.y+=10;
            if(player2life>3){
                player2life -= 3;
            }else{
                player2life=0;
            }
            game.camera.shake(0.02, 300);
            game.camera.flash(0xff0000, 100);
            this.player2Sound.play();
            this.nailsSound.play();

        }
        
    },


    effect: function(player1 , platform) {
        if(platform.key == 'conveyorRight') {
            
            if(player1.x<=560)player1.body.x += 2;
            if (this.player1.touchOn !== platform) {
                this.conveyorSound.play();
                if(player1life < 10) {
                    player1life += 1;
                }
                this.player1.touchOn = platform;
            }
        }
        if(platform.key == 'conveyorLeft') {
            
            if(player1.x>=40)player1.body.x -= 2;
            if (this.player1.touchOn !== platform) {
                if(player1life < 10) {
                    player1life += 1;
                }
                this.conveyorSound.play();
                this.player1.touchOn = platform;
            }
        }
        if(platform.key == 'trampoline') {
            if(player1life < 10) {
                player1life += 1;
            }
            platform.animations.play('jump');
            player1.body.velocity.y = -350;
            this.trampolineSound.play();

        }
        if(platform.key == 'nails') {
            if (this.player1.touchOn !== platform) {
                if(player1life>3){
                    player1life -= 3;
                }else{
                    player1life=0;
                }
                this.player1.touchOn = platform;
                game.camera.shake(0.02, 300);
                game.camera.flash(0xff0000, 100);
                this.playerSound.play();
                this.nailsSound.play();
            }

        }
        if(platform.key == 'plat') {
            if (this.player1.touchOn !== platform) {
                if(player1life < 10) {
                    player1life += 1;
                }
                this.platSound.play();
                this.player1.touchOn = platform;
            }
        }
        if(platform.key == 'fake') {
            if(this.player1.touchOn !== platform) {
                platform.animations.play('turn');
                setTimeout(function() {
                    platform.body.checkCollision.up = false;
                }, 100);
                this.fakeSound.play();
                this.player1.touchOn = platform;
            }
        }

    },
    effect2: function(player2 , platform) {
        if(platform.key == 'conveyorRight') {
            
            if(player2.x<=560)player2.body.x += 2;
            if (this.player2.touchOn !== platform) {
                this.conveyorSound.play();
                if(player2life < 10) {
                    player2life += 1;
                }
                this.player2.touchOn = platform;
            }
        }
        if(platform.key == 'conveyorLeft') {
            
            if(player2.x>=40)player2.body.x -= 2;
            if (this.player2.touchOn !== platform) {
                if(player2life < 10) {
                    player2life += 1;
                }
                this.conveyorSound.play();
                this.player2.touchOn = platform;
            }
        }
        if(platform.key == 'trampoline') {
            if(player2life < 10) {
                player2life += 1;
            }
            platform.animations.play('jump');
            player2.body.velocity.y = -350;
            this.trampolineSound.play();

        }
        if(platform.key == 'nails') {
            if (this.player2.touchOn !== platform) {
                if(player2life>3){
                    player2life -= 3;
                }else{
                    player2life=0;
                }
                this.player2.touchOn = platform;
                game.camera.shake(0.02, 300);
                game.camera.flash(0xff0000, 100);
                this.player2Sound.play();
                this.nailsSound.play();
            }

        }
        if(platform.key == 'plat') {
            if (this.player2.touchOn !== platform) {
                if(player2life < 10) {
                    player2life += 1;
                }
                this.platSound.play();
                this.player2.touchOn = platform;
            }
        }
        if(platform.key == 'fake') {
            if(this.player2.touchOn !== platform) {
                platform.animations.play('turn');
                setTimeout(function() {
                    platform.body.checkCollision.up = false;
                }, 100);
                this.fakeSound.play();
                this.player2.touchOn = platform;
            }
        }

    },        
    movePlayer: function() {
        if (this.cursor.left.isDown) {
            if(this.player1.x>=40)this.player1.x -= 5;
            this.player1.scale.x=-1;
            this.player1.facingLeft = true;
            this.player1.animations.play('leftwalk'); 

        }
        else if (this.cursor.right.isDown) { 
            if(this.player1.x<=560)this.player1.x += 5;
            this.player1.scale.x=1;
            this.player1.facingLeft = false;
            this.player1.animations.play('rightwalk'); 

        }    
        else {
            this.player1.body.velocity.x = 0;
            if(this.player1.facingLeft) {

                this.player1.frame = 0;
                this.player1.scale.x=-1;
            }else {

                this.player1.frame = 0;
                this.player1.scale.x=1;
            }

            this.player1.animations.stop();
        }

    },
    movePlayer2: function() {
        if (key1.isDown) {
            if(this.player2.x>=40)this.player2.x -= 5;
            this.player2.scale.x=-1;
            this.player2.facingLeft = true;
            this.player2.animations.play('leftwalk2'); 

        }
        else if (key2.isDown) { 
            if(this.player2.x<=560)this.player2.x += 5;
            this.player2.scale.x=1;
            this.player2.facingLeft = false;
            this.player2.animations.play('rightwalk2'); 

        }    
        else {
            this.player2.body.velocity.x = 0;
            if(this.player2.facingLeft) {

                this.player2.frame = 0;
                this.player2.scale.x=-1;
            }else {

                this.player2.frame = 0;
                this.player2.scale.x=1;
            }

            this.player2.animations.stop();
        }

    },
    updatePlatforms :function() {
        for(var i=0; i<platforms.length; i++) {
            var platform = platforms[i];
            platform.body.position.y -= speed;
            if(platform.body.position.y <= -20) {
                platform.destroy();
                platforms.splice(i, 1);
            }
        }
    },
    createPlatforms :function() {
        if(game.time.now > lastTime + floortime) {
            lastTime = game.time.now;
            this.createOnePlatform();
            floor += 1;
            floorct +=1;
        }
    },

    createOnePlatform :function() {
    
        var platform;
        var x = Math.random()*(600 - 96 - 40) + 20;
        var y = 500;
        var rand = Math.random() * 100;
    
        if(rand < 20) {
            platform = game.add.sprite(x, y, 'plat');
        } else if (rand < 40) {
            platform = game.add.sprite(x, y, 'nails');
            game.physics.arcade.enable(platform);
            platform.body.setSize(96, 15, 0, 15);
        } else if (rand < 50) {
            platform = game.add.sprite(x, y, 'conveyorLeft');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } else if (rand < 60) {
            platform = game.add.sprite(x, y, 'conveyorRight');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } else if (rand < 80) {
            platform = game.add.sprite(x, y, 'trampoline');
            platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
            platform.frame = 3;
        } else {
            platform = game.add.sprite(x, y, 'fake');
            platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
        }
    
        game.physics.arcade.enable(platform);
        platform.body.immovable = true;
        platforms.push(platform);
        platform.body.checkCollision.down = false;
        platform.body.checkCollision.left = false;
        platform.body.checkCollision.right = false;
    }
    
}
;





