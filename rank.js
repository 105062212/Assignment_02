var sorrow;
var player1a;
var player2a;
var text12;
var text13;
var text14;
var text22;
var text23;
var text24;
var one;
var two;
var three;
var one1;
var two1;
var three1;
var rankState = {

    create: function() {
        sorrow = game.add.audio('Sorrow');
        sorrow.play();
        chooseSound = game.add.audio('start');
        var a=[];
        var b=[];
        var c=[];
        var d=[];
        var e=[];
        var postsRef = firebase.database().ref('1player').orderByChild('floor').limitToLast(3);
        postsRef.once('value').then(function(snapshot){
            snapshot.forEach(function(childSnapshot) {
              var data = childSnapshot.val();
              a.push(data.floor.toString());
              b.push(data.playername.toString());
            });
        })
        var postsRef = firebase.database().ref('2player').orderByChild('floor').limitToLast(3);
        postsRef.once('value').then(function(snapshot){
            snapshot.forEach(function(childSnapshot) {
              var data = childSnapshot.val();
              c.push(data.floor.toString());
              d.push(data.player1name.toString());
              e.push(data.player2name.toString());
            });
        })
        setTimeout(function() {
            game.add.image(0, 0,'ranking');
            var style = {fill: '#0033cc', fontSize: '20px'}
            var style2 = {fill: '#0033cc', fontSize: '50px'}
            var astyle = {fill: '#ff0000', fontSize: '20px'}
            var astyle2 = {fill: '#ff0000', fontSize: '50px'}
            player1a = game.add.text(100, 120, '', style2);
            text12 = game.add.text(100, 200, '', style);
            text13 = game.add.text(100, 250, '', style);
            text14 = game.add.text(100, 300, '', style);
            player2a = game.add.text(350, 120, '', astyle2);
            text22 = game.add.text(350, 200, '', astyle);
            text23 = game.add.text(350, 250, '', astyle);
            text24 = game.add.text(350, 300, '', astyle);
            one = b[2]+' '+a[2]+'層';
            two = b[1]+' '+a[1]+'層';
            three = b[0]+' '+a[0]+'層';
            one1 = d[2]+'&'+e[2]+' '+c[2]+'層';
            two1 = d[1]+'&'+e[1]+' '+c[1]+'層';
            three1 = d[0]+'&'+e[0]+' '+c[0]+'層';
            player1a.setText('1player');
            player2a.setText('2player');
            text12.setText('1.'+one);
            text13.setText('2.'+two);
            text14.setText('3.'+three);
            text22.setText('1.'+one1);
            text23.setText('2.'+two1);
            text24.setText('3.'+three1);
            button = game.add.button(game.width-200 , game.height-60, 'b4', OnClick23, this, 1, 0, 1);
         }, 2000);
       
        
    },

    update: function() {
         
    },

}
function OnClick23 () {
    sorrow.stop();
    chooseSound.play();
    game.state.start('menu');
}
